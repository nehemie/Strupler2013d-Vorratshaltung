## Licence

  - For citation refers to **Strupler 2013** (see [references](#references) )


## Format of the .tsv

  - Unicode (UTF-8)
  - Field delimiter {tab}
  - Text delimiter {"}


## Table 

### [MBAPotteryCapacity.tsv](MBAPotteryCapacity.tsv)

Data frame that contains measurements of MBA vessels from Boğazköy. The data have
been calculated with the program Software `Pot Utility 1.05`, Copyright J. P.
Thalmann & ARCANE 2006 with the images in the folder [drawings](drawings).  The table
[MBAPotteryCapacity-english.tsv](MBAPotteryCapacity-english.tsv) is a
translation.

The variables are
  - **ID**: identifier for the ceramic
  - **Type**: according to a basic typology
  - **Preservation**: state of preservation of the vessel (whole profiles or fragments) 
  - **Capacity**: volume (in litre)
  - **Dop**: diameter of the rim in cm
  - **Dmin**: minimal diameter of the vessel in cm
  - **Dmax**: maximum diameter of the vessel in cm
  - **Dbase**: diameter of the base in cm
  - **Height**: height of the vessel in cm
  - **Stratigraphie**: kind of layer
  - **Origin**: where the object was found
  - **Reference**: publication of the vessel with drawing

Details on the German abbreviation and English  translation are provided in an
additional table,
[TranslationAndEncodingNotes.tsv](TranslationAndEncodingNotes.tsv). 


### [CapacityPitGeb.tsv](CapacityPitGeb.tsv)

This data frame is the original from the article


### [TranslationAndEncodingNotes.tsv](TranslationAndEncodingNotes.tsv)

This file provides ontology for the translation from the [ original German
terminology ](MBAPotteryCapacity-German.tsv) into an [ English version
](MBAPotteryCapacity.tsv)




## References

   - **Fischer 1963**: [Fischer Franz, Die hethitische Keramik von Boğazköy,
     1963](http://amar.hsclib.sunysb.edu/cdm/compoundobject/collection/amar/id/136079)

   - **Orthmann 1984**: Orthmann, Winfried, Keramik aus den ältesten Schichten
     von Büyükkale, in Bittel, K., Bachmann, H.-G., Naumann, R., Neumann, G.,
Neve, P., Orthmann, W. & Otten, H. Boğazköy VI. Funde aus den Grabungen bis
1979, 1984, 9-62

   - **Schachner 2010**: Schachner, A. Die Ausgrabungen in Boğazköy-Ḫattuša
     2010, Archäologischer Anzeiger 2011, 31-86 

   - **Schachner 2012**: Schachner, A. Die Ausgrabungen in Boğazköy-Ḫattuša
     2011, Archäologischer Anzeiger 2012, 85-13 

   - **Strupler 2011**: [Strupler, Néhémie, Vorläufiger Überblick über die
     Karum-zeitlichen Keramikinventare aus den Grabungen in der südlichen
Unterstadt apud Schachner, Andreas, Die Ausgrabungen in 2010, Archäologischer
Anzeiger 2011/1, 51 - 57](https://orcid.org/0000-0002-2898-6217)

   - **Strupler 2013**: [Strupler Néhémie, Vorratshaltung im
     mittelbronzezeitlichen Bogazköy. Spiegel einer häuslichen und regionalen
Ökonomie Istanbuler Mitteilungen, 2013, 63,
17-50](https://orcid.org/0000-0002-2898-6217)




### As BibTeX

    @Book{Fischer1963,
      Title           = {{D}ie hethitische {K}eramik von {B}o\u{g}azköy},
      Author          = {Fischer, Franz},
      Publisher       = {Mann},
      Year            = {1963},
      Address         = {Berlin}
    }
    
    @InCollection{Orthmann1984,
      Title           = {{K}eramik aus den ältesten {S}chichten von {B}üyükkale},
      Author          = {Orthmann, Winfried},
      Year            = {1984},
      Address         = {Berlin},
      Pages           = {9-62},
      Crossref        = {Bittel1984}
    }
    
    @Book{Bittel1984,
      Title           = {{B}o\u gazköy {VI}. {F}unde aus den {G}rabungen bis 1979},
      Author          = {Bittel, Kurt and Bachmann, Hans-Gert and Naumann,
                         Rudolf and Neumann, G{\"u}nter and Neve, Peter and
                         Orthmann, Winfried and Otten, Heinrich},
      Publisher       = {Mann},
      Year            = {1984},
      Address         = {Berlin}
    }
    
    @Article{Schachner2010,
      Title           = {{D}ie {A}usgrabungen in {B}o\u gazköy--Hattu\v{s}a 2009},
      Author          = {Schachner, Andreas},
      Journal         = {Archäologischer Anzeiger},
      Year            = {2010},
      Pages           = {161- 221}
    }
    
    @Article{Schachner2011,
      Title           = {{D}ie {A}usgrabungen in  {B}o\u gazköy--Hattu\v{s}a 2010},
      Author          = {Schachner, Andreas},
      Journal         = {Archäologischer Anzeiger},
      Year            = {2011},
      Pages           = {31-86}
    }
    
    
    @Article{Schachner2012,
      Title           = {{D}ie {A}usgrabungen in {B}o\u gazköy--Hattu\v{s}a 2011},
      Author          = {Schachner, Andreas},
      Journal         = {Archäologischer Anzeiger},
      Year            = {2012},
      Pages           = {85-137}
    }
    
    @Article{Strupler2011,
      Title           = {{V}orl{\"a}ufiger {Ü}berblick über die {K}arum-zeitlichenr
                        {K}eramikinventare aus den {G}rabungen in der südlichen {U}nterstadt},
      Author          = {Strupler, Néhémie},
      Journal         = {Archäologischer Anzeiger},
      Year            = {2011},
      Pages           = {51-57},
      Crossref        = {Schachner2011}
    }
    
    @Article{Strupler2013,
      Title           = {Vorratshaltung im mittelbronzezeitlichen Bogazköy.
                         Spiegel einer häuslichen und regionalen Ökonomie},
      Author          = {Néhémie Strupler},
      Journal         = {Istanbuler Mitteilungen},
      Year            = {2013},
      Pages           = {17--50},
      Volume          = {63}
    }

